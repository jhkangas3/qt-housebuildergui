import QtQuick 2.15
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Window
import QtQuick.Controls.Material

// Main window
ApplicationWindow {
    width: 1000
    height: 800
    minimumWidth: 1000
    minimumHeight: 800
    visible: true
    title: "Housebuilder3000"
    id: menuPage

    // Header with shutdown button
    header: ToolBar
    {
        Rectangle
        {
            width:48
            height:48
            radius:24
            color: Material.accent
            anchors.right:parent.right

            RoundButton
            {
                anchors.fill: parent
                Image
                {
                    anchors.fill: parent
                    id: shutdownIcon
                    source: "qrc:/images/shutdown-icon.png"
                }

                onClicked:
                {
                    Qt.quit()
                }
            }
        }
    }
    // Initial StackView page
    StackView
    {
        id: stackView
        anchors.margins: 20
        anchors.fill: parent

        initialItem: Pane
        {
            id: pane

            //Layout with two house types
            ColumnLayout
            {
                width: parent.width
                height: parent.height
                anchors.fill: parent
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                RowLayout
                {
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                    Button
                    {
                        id:house
                        Layout.preferredHeight: 350; Layout.preferredWidth: 300
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                        Image
                        {
                            id: houseOne
                            anchors.fill: parent
                            anchors.centerIn: parent
                            source: "qrc:/images/first_house.png"

                        }
                    }

                    Button
                    {
                        id:apartment

                        Layout.preferredHeight: 350; Layout.preferredWidth: 300
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                        Image
                        {
                            id: apartmentOne
                            anchors.fill: parent
                            anchors.centerIn: parent

                            source: "qrc:/images/first_apartment.png"
                        }
                    }

                }

                // Layout with page navigation buttons
                RowLayout
                {
                    anchors.fill: parent

                    Rectangle
                    {
                        id:nextPage

                        width:100
                        height:100
                        radius:50
                        anchors.right:parent.right
                        anchors.bottom:parent.bottom
                        color: Material.accent

                        RoundButton
                        {
                            anchors.fill:parent
                            Text
                            {
                                anchors.centerIn: parent
                                text: "Next"
                            }
                            onClicked:
                            {
                                stackView.push("qrc:/second_window.qml")
                            }
                        }
                    }

                    Label
                    {
                       Layout.fillWidth: true
                    }

                    Rectangle
                    {
                        id:previousPage

                        width:100
                        height:100
                        radius:50
                        anchors.left:parent.left
                        anchors.bottom:parent.bottom
                        color: Material.accent

                        RoundButton
                        {
                            anchors.fill:parent
                            Text
                            {
                                anchors.centerIn: parent
                                text: "Quit"
                            }
                            onClicked:
                            {
                                Qt.quit()
                            }
                        }
                    }
                }
            }
        }
    }
}

