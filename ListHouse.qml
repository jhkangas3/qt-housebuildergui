import QtQuick 2.0

ListModel {

    ListElement {
        name: "Soviet cube"
        source: "qrc:/images/first_apartment.png"
    }

    ListElement {
        name: "New apartment"
        source: "qrc:/images/second_apartment.png"
    }

    ListElement {
        name: "Empire State Building"
        source: "qrc:/images/first_apartment.png"
    }

    ListElement {
        name: "Purple house"
        source: "qrc:/images/second_apartment.png"
    }
    ListElement {
        name: "Black house"
        source: "qrc:/images/first_apartment.png"
    }

    ListElement {
        name: "Red cube"
        source: "qrc:/images/second_apartment.png"
    }


}
