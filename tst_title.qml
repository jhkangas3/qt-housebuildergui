import QtQuick
import QtTest

TestCase
{
    id: titleTest
    name: "testTitle"

    function test_title() {
        var component = Qt.createComponent("main.qml")
        var obj = component.createObject(top)
        compare(obj.title, ("Housebuilder3000"));
        obj.destroy()
        component.destroy()
    }
}
