## NAME
Qt: Assignment 7

## DESCRIPTION
Housebuilder GUI

## PROGRAM FAQ

This is a mock up GUI of massive 3D printer for building houses.

Program is with Qt Creator using QML.

Program is usis StackView with 3 different views.
1. Start page, Choose building type
2. Choose building
3. Build bulding


## AUTHORS
Tino Nummela | [@tinonum](https://gitlab.com/tinonum)

Juho Kangas | [@jhkangas3](https://gitlab.com/jhkangas3)
