import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Window
import QtQuick.Controls.Material

// Second page
Page {
    id: secondPage
    visible: true

    //Layout with available buildings. Reading from ListModel and using ListView to print images
    ColumnLayout {

        anchors.fill: parent
        anchors.margins: 20
        Label {
            text: "Choose building"
            Layout.alignment: Qt.AlignHCenter
            font.pixelSize: 20
        }

        ButtonGroup {
            id: radioGroup
        }
        Frame{
            id: filesFrame
            leftPadding: 1
            rightPadding: 1
            Layout.fillWidth: true
            Layout.fillHeight: true
            ListView {
                id: houseListView
                clip: true
                anchors.fill: parent

                model: ListHouse {}
                delegate: RowLayout {
                    width: ListView.view.width
                    RadioButton {
                        text: model.name
                        ButtonGroup.group: radioGroup
                    }
                    Label { }
                        Layout.fillWidth: true
                    Image {
                        Layout.alignment: Qt.AlignRight
                        sourceSize.width: 100; sourceSize.height: 100
                        fillMode: Image.PreserveAspectFit
                        source: model.source

                    }
                }

                ScrollBar.vertical: ScrollBar {
                    parent: filesFrame
                    policy: ScrollBar.AlwaysOn
                    anchors.top: parent.top
                    anchors.topMargin: filesFrame.topPadding
                    anchors.right: parent.right
                    anchors.rightMargin: 1
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: filesFrame.bottomPadding
                    }
               }
        }

        // Row layout with navigation buttons
        RowLayout
        {
            anchors.fill: parent
            Rectangle
            {
                id: nextPage

                width: 100
                height: 100
                radius: 50
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                color: Material.accent

                RoundButton
                {
                    anchors.fill: parent

                    Text
                    {
                        anchors.centerIn: parent
                        text: "Build"
                    }

                    onClicked:
                    {
                        stackView.push("qrc:/third_window.qml")

                    }
                }
            }

            Rectangle
            {
                id:previousPage

                width:100
                height:100
                radius:50
                anchors.left:parent.left
                anchors.bottom:parent.bottom
                color: Material.accent

                RoundButton
                {
                    anchors.fill:parent

                    Text
                    {
                        anchors.centerIn: parent
                        text: "Back"
                    }

                    onClicked:
                    {
                        stackView.pop()
                    }
                }
            }
        }
    }
}

