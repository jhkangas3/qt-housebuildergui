import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Window
import QtQuick.Controls.Material

// Third page
Page {
    id: thirdPage
    visible: true
    title: qsTr("Choose building")
    ColumnLayout {
        anchors.fill: parent
        anchors.centerIn: parent.Center
        Layout.fillWidth: true

        // Row layout to print house animation and material left animation
        RowLayout{
            Layout.fillHeight: true
            Layout.fillWidth: true
            ColumnLayout
            {
                Layout.alignment: Qt.AlignHCenter
                Layout.fillWidth: true
                Layout.margins: 30

                Layout.leftMargin: 210



                Image {
                    Layout.alignment: Qt.AlignHCenter | Qt.AlignTop
                    sourceSize.height: 300
                    sourceSize.width: 200
                    Layout.fillHeight: true
                    //Layout.fillWidth: true

                    height: parent.height
                    width: parent.width

                    id: house
                    source:"qrc:/images/first_apartment.png"
                    scale: 0
                    clip: true

                    PropertyAnimation {
                        id: buildingAnimation
                        running: false
                        target: house
                        properties: "height"
                        from: 0
                        to: house.height
                        duration: 5000
                    }
                }
            }

            ColumnLayout {
                Layout.alignment: Qt.AlignRight | Qt.AlignTop
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.margins: 30
                Layout.rightMargin: 210

                Label {
                    text: "Remaining material"
                }

                Rectangle
                    {
                        width: 200
                        height: 300
                        id: materialAnimation1
                        color: "green"
                    }

                    Rectangle
                    {
                        parent: materialAnimation1
                        width: materialAnimation1.width; height: 0
                        id: materialAnimation
                        color: "red"
                    }

                    PropertyAnimation
                    {
                        id: animateColor
                        target: materialAnimation
                        properties: "height"
                        to: materialAnimation1.height
                        duration: 5000
                    }
            }
        }

        // Row layout with navigation button and build button which starts animations
        RowLayout
        {

            Layout.alignment: Qt.AlignBottom
            Layout.margins: 20

            Rectangle
            {
                id:previousPage

                width:100
                height:100
                radius:50
                color: Material.accent

                Layout.alignment: Qt.AlignBottom

                RoundButton
                {
                    anchors.fill:parent

                    Text
                    {
                        anchors.centerIn: parent
                        text: "Back"
                    }

                    onClicked:
                    {
                        stackView.pop()
                    }
                }
            }

            Label {
                Layout.fillWidth: true
            }

            Rectangle
            {
                id: nextPage

                width: 100
                height: 100
                radius: 50

                color: Material.accent

                Layout.alignment: Qt.AlignBottom

                RoundButton
                {
                    anchors.fill: parent

                    Text
                    {
                        anchors.centerIn: parent
                        text: "Build"
                    }

                    onClicked:
                    {
                        house.scale = 1
                        animateColor.start()
                        buildingAnimation.start()
                    }
                }
            }
        }
    }
}
